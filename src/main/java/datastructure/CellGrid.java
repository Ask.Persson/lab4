package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows;
    private int columns;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;
        grid = new CellState[rows][columns];
        for(int row=0; row<rows; row++){
            for(int col=0; col<columns; col++){
                grid[row][col]=initialState;
            }
        }

	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub

        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if(row<0 || row >= rows)
            throw new IndexOutOfBoundsException("Illegal row value");
        if(column<0 || column >= columns)
            throw new IndexOutOfBoundsException("Illegal column value");
        grid [row][column] = element;



    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if(row<0 || row >= rows)
            throw new IndexOutOfBoundsException("Illegal row value");
        if(column<0 || column >= columns)
            throw new IndexOutOfBoundsException("Illegal column value");
        return grid [row][column];

    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid gridCopy = new CellGrid(this.rows, this.columns, CellState.DEAD);
        for(int row=0; row<rows; row++){
            for(int col=0; col<columns; col++){
                gridCopy.set(row, col, grid[row][col]);

    }}
        return gridCopy;
    }
}
